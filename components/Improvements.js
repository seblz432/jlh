import Components from './index'
import SbEditable from 'storyblok-react'

export default (props) => (
  <SbEditable content={props.content}>
    <div className="improvements">
      <span className="improvements-title">LIST OF IMPROVEMENTS</span>
      <div className="improvements_separator"/>

      <div className="improvements-improvements">
        {props.content.improvements.map((blok) =>
          Components(blok)
        )}
      </div>
    </div>

    <style jsx>{`
      .improvements {
        margin-top: 50px;
      }
      .improvements-improvements {
        display: flex;
        flex-direction: row;
        margin-top: 40px;
        flex-wrap: wrap;
      }
      .improvements-title {
        font-size: 25px;
        color: #A0892C;
      }
      .improvements_separator {
        height: 13px;
        width: 234.5px;
        border-bottom: 1px solid #B3B3B3;
      }

      /* MOBILE LAYOUT */
      @media only screen and (max-width: 1092px) {
        .improvements {
          display: flex;
          flex-direction: column;
          align-items: center;
          margin-top: 25px;
          margin-bottom: 20px;
        }
        .improvements-improvements {
          justify-content: center;
        }
      }
    `}</style>
  </SbEditable>
)
