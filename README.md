# JLH Capital Partners | Featured turnkey page

This is a page on the [JLH Capital Partners' Website](https://featured.jlhcapitalpartners.com/) to show the current featured turnkey. Design was done by John Poh and implemented by me. This website uses [Next.js](https://nextjs.org/) on the frontend and [Storyblok](https://www.storyblok.com) for the CMS (content management system).

## Run the project
Set the preview domain in <strong>Storyblok</strong> to `http://localhost:3000`

```sh
# to run in developer mode
$ yarn dev # or npm run dev
```

```sh
# to build your project
$ yarn build # or npm run build
```

For a detailed explanation on how things work, checkout the [Next.js docs](https://nextjs.org/docs/#setup).

