import Components from './index'
import SbEditable from 'storyblok-react'

export default (props) => (
  <SbEditable content={props.content}>
    <div className="vertical_layout">
      {props.content.components.map((blok) =>
        Components(blok)
      )}
    </div>

    <style jsx>{`
      .vertical_layout {
        margin-left: 20px;
        margin-right: 20px;
        display: flex;
        flex-direction: column;
      }
      @media only screen and (max-width: 1400px) {
        .vertical_layout {
          margin-left: 9px;
          margin-right: 9px;
        }
      }
    `}</style>
  </SbEditable>
)
