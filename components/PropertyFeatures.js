import Components from './index'
import SbEditable from 'storyblok-react'

export default (props) => (
  <SbEditable content={props.content}>
    <div className="property_features">
      <span className="features-title">PROPERTY FEATURES</span>
      <div className="overview-address_separator"/>

      <div className="features-container">
        <span className="feature">SQUARE FOOTAGE <br/> {props.content.square_footage}</span>

        <span className="feature">YEAR BUILT <br/> {props.content.year_built}</span>

        <span className="feature">NUMBER OF BEDS <br/> {props.content.number_of_beds}</span>

        <span className="feature">NUMBER OF BATHS <br/> {props.content.number_of_baths}</span>
      </div>
    </div>

    <style jsx>{`
      .property_features {

      }
      .features-container {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        margin-top: 31px;
      }
      .features-title {
        font-size: 25px;
        color: #A0892C;
      }
      .overview-address_separator {
        height: 13px;
        width: 234.5px;
        border-bottom: 1px solid #B3B3B3;
      }
      .feature {
        font-size: 20px;
        margin-right: 100px;
        margin-bottom: 37px;
        width: 190px;
      }

      /* MOBILE LAYOUT */
      @media only screen and (max-width: 1092px) {
        .property_features {
          display: flex;
          flex-direction: column;
          align-items: center;
        }
        .features-container {
          justify-content: space-between;
        }
        .feature {
          margin-right: 50px;
          margin-left: 50px;
          width: 190px;
        }
      }

      /* SMALLER MOBILE LAYOUT */
      @media only screen and (max-width: 703px) {
        .features-container {
          width: 290px;
        }
      }
    `}</style>
  </SbEditable>
)
