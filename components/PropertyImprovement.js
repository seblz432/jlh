import Components from './index'
import SbEditable from 'storyblok-react'

export default (props) => (
  <SbEditable content={props.content}>
    <div className="property_improvement">
      <img className="improvement-logo" src={props.content.icon.filename}/>
      <span>{props.content.name}</span>
      <p>{props.content.details}</p>
    </div>

    <style jsx>{`
      .property_improvement {
        margin-right: 70px;
        padding-bottom: 15px;
        display: flex;
        flex-direction: column;
        align-items: center;
      }
      .improvement-logo {
        margin-bottom: 5px;
        height: 50px;
        width: 85px;
      }
      p {
        white-space: pre-wrap;
        font-size: 15px;
      }

      /* MOBILE LAYOUT */
      @media only screen and (max-width: 1092px) {
        .property_improvement {
          margin-right: 35px;
          margin-left: 35px;
        }
      }
    `}</style>
  </SbEditable>
)
