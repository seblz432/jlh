import Components from './index'
import SbEditable from 'storyblok-react'

export default (props) => (
  <SbEditable content={props.content}>
    <div className="investment_metrics">
      <span className="metrics-title">INVESTMENT METRICS</span>
      <div className="metrics-separator"/>

      {/*console.log(props.content.years)*/}

      <div className="metrics-tabs">
        {/* Generate tabs by mapping the years from the Storyblok api */
          props.content.years.map((year, index) => {
            return (
              <div key={index}>
                <input className="metrics-radio" type="radio" name="years" id={"tab" + index} defaultChecked={(index === 0) ? 'defaultChecked' : '' } />
                <label htmlFor={"tab" + index} className="metrics-tab"><span>YEAR {year.year}</span></label>

                <div className="metrics-tab-content">

                  <span className="metrics-tab-item">
                    CURRENT RENT <br/> ${parseFloat(year.current_rent).toLocaleString('en')}
                  </span>
                  <span className="metrics-tab-item">
                    GROSS YIELD <br/> {parseFloat(year.gross_yield).toLocaleString('en')}%
                  </span>
                  <span className="metrics-tab-item">
                    NOI <br/> ${parseFloat(year.noi).toLocaleString('en')}
                  </span>
                  <span className="metrics-tab-item">
                    GRM <br/> {parseFloat(year.grm).toLocaleString('en')}
                  </span>
                  <span className="metrics-tab-item">
                    CAPITALIZATION RATE <br/> {parseFloat(year.capitalization_rate).toLocaleString('en')}%
                  </span>
                  <span className="metrics-tab-item">
                    NEIGHBORHOOD RANK <br/> {year.neighborhood_rank}
                  </span>

                </div>
              </div>
            );
          }
        )}
      </div>

    </div>

    <style jsx>{`
      .investment_metrics {
        margin-left: 20px;
        margin-right: 20px;
        margin-top: 0px;
        padding-top: 29px;
        padding-bottom: 70px;
        padding-left: 47px;
        padding-right: 47px;
        border: 1px solid #B3B3B3;
        border-top: 10px solid #B3B3B3;
      }
      @media only screen and (max-width: 1400px) {
        .investment_metrics {
          margin-left: 9px;
          margin-right: 9px;
        }
      }
      .metrics-title {
        font-size: 25px;
        color: #A0892C;
      }
      .metrics-separator {
        height: 13px;
        width: 234.5px;
        border-bottom: 1px solid #B3B3B3;
      }
      .metrics-tabs {
        display: flex;
        flex-direction: row;
        margin-top: 20px;
        margin-left: -5px;
        position: relative;
      }
      .metrics-tab {
        display: flex;
        flex-direction: column;
        justify-content: center;
        margin-left: 5px;
        margin-right: 5px;
        width: 138px;
        height: 32px;
        font-size: 20px;
        text-align: center;

        color: black;
        background: #fff;

        border-style: solid;
        border-width: 1px;
        border-color: #717171;

        position: absolute;
        top: 5px;
      }
      .metrics-tab-content {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        font-size: 20px;
        margin-top: 32px;
        display: none;
      }
      .metrics-tab-item {
        margin-top: 38px;
        margin-right: 129px;
        width: 250px;
      }

      /* TABS */
      .metrics-radio:checked + .metrics-tab, .metrics-radio:checked + .metrics-tab > span {
        color: white;
        background: #717171;
      }
      .metrics-radio:checked + .metrics-tab + .metrics-tab-content {
        display: flex;
      }
      input[type="radio"] {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
      }
      #tab0 + label {
        left: -4px;
      }
      #tab1 + label {
        left: 144px;
      }

      /* MOBILE LAYOUT */
      @media only screen and (max-width: 1092px) {
        .investment_metrics {
          display: flex;
          flex-direction: column;
          align-items: center;
        }
        .metrics-tab-content {
          justify-content: space-between;
          width: 100%;
        }
        .metrics-tab-item {
          margin-right: 30px;
          margin-left: 30px;
        }
        #tab0 + label {
          left: calc(50% - 148px);
        }
        #tab1 + label {
          left: 50%;
        }
      }

      /* SMALLER MOBILE LAYOUT */
      @media only screen and (max-width: 746px) {
        .metrics-tabs {
          justify-content: center;
          align-items: center;
        }
        .metrics-tab-content {
          justify-content: space-between;
          width: 345px;
        }
      }
    `}</style>
  </SbEditable>
)
