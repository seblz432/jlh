import Components from './index'
import SbEditable from 'storyblok-react'

export default (props) => (
  <SbEditable content={props.content}>
    <div className="horizontal_divider"/>

    <style jsx>{`
      .horizontal_divider {
        border-top: 1px solid #B3B3B3;
        margin-top: 42px;
        margin-bottom: 45px;
        margin-left: 40px;
        margin-right: 40px;
      }
      @media only screen and (max-width: 1400px) {
        .horizontal_divider {
          margin-left: 18px;
          margin-right: 18px;
        }
      }
    `}</style>
  </SbEditable>
)
