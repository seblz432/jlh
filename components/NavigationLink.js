import Components from './index'
import SbEditable from 'storyblok-react'
import Link from "next/link";

export default (props) => (
  <SbEditable content={props.content}>
    <a href={props.content.link.url}>
      <span>
        {props.content.name.toUpperCase()}
      </span>
    </a>

    <style jsx>{`
      a {
        font-size: 24px;
        margin-left: 125px;
        text-decoration: none;
      }
      /* MOBILE LAYOUT */
      @media only screen and (max-width: 1400px) {
        a {
          margin-left: 0;
          width: 100%;
          font-size: 20px;
        }
      }
    `}</style>
  </SbEditable>
)
