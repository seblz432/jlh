import Components from './index'
import SbEditable from 'storyblok-react'

export default (props) => (
  <SbEditable content={props.content}>
    <div className="property">
      {props.content.body.map((blok) =>
        Components(blok)
      )}
    </div>

    <style jsx>{`
      /* ALL THESE PADDINGS ARE HALVED SO THAT THE CHILDREN ALSO HAVE HALF IN THEIR MARGINS */

      .property {
        padding-left: 20px;
        padding-right: 20px;
      }
      /* MOBILE LAYOUT */
      @media only screen and (max-width: 1400px) {
        .property {
          padding-left: 9px;
          padding-right: 9px;
        }
      }
    `}</style>
  </SbEditable>
)
