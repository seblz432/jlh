import Components from './index'
import SbEditable from 'storyblok-react'

export default (props) => (
  <SbEditable content={props.content}>
    <div className="horizontal_layout">
      {props.content.components.map((blok) =>
        Components(blok)
      )}
    </div>

    <style jsx>{`
      .horizontal_layout {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        justify-content: center;
      }
    `}</style>
  </SbEditable>
)
