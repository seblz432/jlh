import Components from './index'
import SbEditable from 'storyblok-react'
import { useState } from 'react';

import MenuIcon from '../public/menu.svg'

const Navigation = (props) => {

  const [menu, setMenu] = useState(false);

  return (
    <SbEditable content={props.content}>
      <div className="navigation">
        <img className="navigation-logo" src={props.content.logo} />

        <div className="navigation-links_container">
            {props.content.links.map((blok) =>
              Components(blok)
            )}
        </div>
        <div onClick={()=>{setMenu(!menu)}} className="navigation-menu_icon">
          <MenuIcon className="menuI"/>
        </div>
      </div>
      <div className="mobileMenu" id={ (menu) ? '' : 'mobileMenuHidden' }>
        {props.content.links.map((blok) =>
          Components(blok)
        )}
      </div>

      <style jsx>{`
        .navigation {
          padding-left: 75px;
          padding-right: 75px;
          margin-bottom: 49px;
          display: flex;
          align-items:center;
          position: sticky;
          top: 0;
          z-index: 10;
          height: 110px;
          background-color: white;
          border-bottom: 14px solid #E6E6E6;
        }
        .navigation-logo {
          width: 112px;
        }
        .navigation-links_container {
          display: flex;
          flex-direction: row;
          margin-right: 0;
          margin-left: auto;
        }
        .navigation-menu_icon {
          margin-top: 8px;
          margin-right: 0;
          margin-left: auto;
          cursor: pointer;
        }
        .mobileMenu {
          display: flex;
          flex-wrap: wrap;
          flex-direction: column;
          justify-content: space-around;
          align-items: center;
          text-align: center;
          position: fixed;

          /*top: 96px;*/
          top: 110px;
          left: 10%;
          padding: 5% 0;
          width: 80%;
          height: 325px;
          max-height: 50%;

          background-color: white;
          border: solid #A0892C;
          border-width: 1px 1px 5px 1px;
          z-index: 10;

          visibility: visible;
          opacity: 1;
          transition-timing-function: ease-out;
          transition: visibility 0.2s, opacity 0.2s;
        }
        /* MOBILE LAYOUT */
        @media only screen and (max-width: 1400px) {
          .navigation-links_container {
            display: none;
          }
          .navigation {
            padding-left: calc(50% - 45px);
            padding-right: 35px;
          }
          .navigation-logo {
            width: 90px;
          }
        }
        @media only screen and (min-width: 1401px) {
          .navigation-menu_icon {
            display: none;
          }
        }
      `}</style>
    </SbEditable>
  )
}

export default Navigation;
