import Components from './index'
import SbEditable from 'storyblok-react'

export default (props) => (
  <SbEditable content={props.content}>
    <div className="overview">
      {/* OVERVIEW SECTION */}
      <div className="property-overview_container">
        {/* ADDRESS */}
        <div className="overview-address_container">
          <span className="overview-street_address">{props.content.street_address}</span>
          <span className="overview-rest_of_address">{props.content.city}, {props.content.state} {props.content.zip_code}</span>
          <div className="overview-address_separator"/>
        </div>
        {/* PRICING INFO */}
        <div className="overview-price_info">
          <div className="overview-sales_info_container">

            <span className="overview-sales_info_text desktop">SALES PRICE ${parseFloat(props.content.sales_price).toLocaleString('en')}</span>

            <span className="overview-sales_info_text mobile">SALES PRICE</span>

            <div className="overview-price_info_separator"/>

            <span className="overview-sales_info_text mobile">${parseFloat(props.content.sales_price).toLocaleString('en')}</span>

          </div>
          <div className="overview-sales_info_spacer"/>
            <div  className="overview-sales_info_container bottom">

              <span className="overview-sales_info_text desktop">MONTHLY RENT ${parseFloat(props.content.monthly_rent).toLocaleString('en')}</span>

              <span className="overview-sales_info_text mobile">MONTHLY RENT</span>

              <div className="overview-price_info_separator"/>

              <span className="overview-sales_info_text mobile">${parseFloat(props.content.monthly_rent).toLocaleString('en')}</span>

            </div>
        </div>
      </div>
    </div>

    <style jsx>{`
      .property-overview_container {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        justify-content: center;
      }
      .overview-address_container {
        display: flex;
        flex-direction: column;
      }
      .overview-street_address {
        font-size: 36px;
        color: #A0892C;
      }
      .overview-rest_of_address {
        font-size: 24px;
        color: #4D4D4D;
      }
      .overview-address_separator {
        height: 13px;
        width: 170.5px;
        border-bottom: 1px solid #B3B3B3;
      }
      .overview-price_info {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        align-items:center;
        margin-left: 40px;
        width: 767px;
        background-color: #3E3E3E;
        padding-left: 30px;
        padding-right: 30px;
      }
      /* MOBILE LAYOUT */
      @media only screen and (max-width: 1092px) {
        .overview-address_container {
          align-items: center;
          margin-bottom: 30px;
        }
        .overview-sales_info_container {
          display: flex;
          flex-direction: column;
          align-items: center;
        }
        .overview-sales_info_container.bottom {
          margin-top: 0;
        }
        .overview-price_info {
          width: 100%;
          margin-left: 18px;
          margin-right: 18px;
          flex-direction: column;
        }
      }
      .overview-sales_info_container {
        margin-top: 25px;
        margin-bottom: 25px;
      }
      .overview-sales_info_text {
        font-size: 25px;
        color: white;
      }
      .overview-price_info_separator {
        height: 5px;
        width: 117px;
        border-bottom: 1px solid #A0892C;
      }
      .overview-sales_info_spacer {
        width: 140px;
      }
    `}</style>
  </SbEditable>
)
