import React from 'react'

import Page from './Page'
import HorizontalLayout from './HorizontalLayout'
import VerticalLayout from './VerticalLayout'
import HorizontalDivider from './HorizontalDivider'

import Navigation from './Navigation'
import NavigationLink from './NavigationLink'

import Property from './Property'

import Overview from './Overview'
import Carousel from './Carousel'
import PropertyInfo from './PropertyInfo'
import PropertyFeatures from './PropertyFeatures'
import Improvements from './Improvements'
import PropertyImprovement from './PropertyImprovement'

import InvestmentMetrics from './InvestmentMetrics'

import MissionStatement from './MissionStatement'
import MailingList from './MailingList'

const Components = {
  'page': Page,
  'horizontal_layout': HorizontalLayout,
  'vertical_layout': VerticalLayout,
  'horizontal_divider': HorizontalDivider,
  'navigation': Navigation,
  'navigation_link': NavigationLink,
  'property': Property,
  'overview': Overview,
  'property_info': PropertyInfo,
  'property_features': PropertyFeatures,
  'improvements': Improvements,
  'property_improvement': PropertyImprovement,
  'investment_metrics': InvestmentMetrics,
  'carousel': Carousel,
  'mission_statement': MissionStatement,
  'mailing_list': MailingList
}

export default (blok) => {
  if (typeof Components[blok.component] !== 'undefined') {
    return React.createElement(Components[blok.component], {key: blok._uid, content: blok})
  }
  return React.createElement(() => (
    <div>The component {blok.component} has not been created yet.</div>
  ), {key: blok._uid})
}
