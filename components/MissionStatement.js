import Components from './index'
import SbEditable from 'storyblok-react'
import StoryblokService from '../utils/StoryblokService'

export default (props) => (
  <SbEditable content={props.content}>
    <div className="mission_statement">
      <span className="mission-title">MISSION STATEMENT</span>
      <div className="mission-separator"/>

      <div className="mission-text"
        dangerouslySetInnerHTML={{
          __html: StoryblokService.client.richTextResolver.render(props.content.body)
        }}
      />
    </div>

    <style jsx global>{`
      .mission_statement {
        flex-grow: 4;
        margin-bottom: 45px;
        margin-left: 40px;
        margin-right: 40px;
        padding-top: 30px;
        padding-bottom: 50px;
        padding-left: 47px;
        padding-right: 47px;
        border: 1px solid #B3B3B3;
        max-width: 1000px;
      }
      /* BREAK POINT */
      @media only screen and (max-width: 1686px) {
        .mission_statement {
          max-width: 100%;
        }
      }
      /* MOBILE LAYOUT */
      @media only screen and (max-width: 1400px) {
        .mission_statement {
          display: flex;
          flex-direction: column;
          align-items: center;
          margin-left: 18px;
          margin-right: 18px;
        }
      }
      .mission-title {
        font-size: 25px;
        color: #A0892C;
      }
      .mission-separator {
        height: 13px;
        width: 231px;
        border-bottom: 1px solid #B3B3B3;
      }
      .mission-text {
        margin-top: 24px;
        font-size: 18px;
      }
    `}</style>
  </SbEditable>
)
