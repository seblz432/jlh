import NextHead from 'next/head'
import Components from '../components/index'
import StoryblokService from '../utils/StoryblokService'
import SbEditable from 'storyblok-react'
import React from 'react'

export default class extends React.Component {
  constructor(props) {
    super(props)
    this.state = {pageContent: props.page.data.story.content}
  }

  static async getInitialProps({ query }) {
    StoryblokService.setQuery(query)
    let slug = query.slug || 'featured'

    return {
      page: await StoryblokService.get(`cdn/stories/${slug}`)
    }
  }

  componentDidMount() {
    StoryblokService.initEditor(this)
  }

  render() {
    return (
      <div>
        <NextHead>
          {StoryblokService.bridge()}
          <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet"/>
          <link rel="icon" href="/favicon.ico?v=3" />
        </NextHead>

        {Components(this.state.pageContent)}

        <style jsx global>{`
          html {
            font-family: 'Lato', sans-serif;
            font-size: 16px;
            word-spacing: 1px;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
            -moz-osx-font-smoothing: grayscale;
            -webkit-font-smoothing: antialiased;
            box-sizing: border-box;
          }

          *, *:before, *:after {
            box-sizing: border-box;
            margin: 0;
          }

          *:focus {
            outline: none;
          }


          .mission-text > p, .mission-text > ul > li {
            margin: 1em 0px;
          }

          ul {
           list-style:square;
           color: #A0892C;
          }

          /* Default text colour */
          p, span {
            color: #666666;
          }

          .teaser,
          .column {
            font-size: 2rem;
            text-align: center;
            line-height: 3;
            background: #ebeff2;
            border-radius: 10px;
            margin: 10px 5px;
          }

          .grid {
            display: flex;
          }

          .column {
            flex: 1;
          }

          .desktop {
            display: block;
          }
          .mobile {
            display: none;
          }

          #mobileMenuHidden {
            visibility: hidden;
            opacity: 0;
          }

          @media only screen and (max-width: 1092px) {
            .desktop {
              display: none;
            }
            .mobile {
              display: block;
            }
          }

          /* MAILING LIST */
          #mc-field-group {
            width: 100%;
          }
          #mc_embed_signup_scroll {
            display: flex;
            margin-top: 24px;
            height: 42px;
            width: 100%;
            max-width: 1000px;
          }
          #mc-embedded-subscribe, #mce-EMAIL {
            height: 100%;
            border-radius: 0;
            border-width: 1px;
            border-style: solid;
          }
          #mce-EMAIL {
            border-color: #B3B3B3;
            width: 100%;
            font-size: 15px;
          }
          #mc-embedded-subscribe {
            padding-top: 9px;
            padding-bottom: 9px;
            padding-left: 40px;
            padding-right: 40px;
            border-color: #A0892C;
            background-color: #A0892C;
            color: white;
            font-family: 'Lato', sans-serif;
            font-size: 20px;
          }
          /* MOBILE LAYOUT */
          @media only screen and (max-width: 560px) {
            .mailing_list {
              padding-left: 30px;
              padding-right: 30px;
            }
            #mc-embedded-subscribe {
              padding-left: 10px;
              padding-right: 10px;
            }
            #mce-EMAIL {
              width: calc(100% - 128px);
            }
          }
        `}</style>
      </div>
    )
  }
}
