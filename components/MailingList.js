import Components from './index'
import SbEditable from 'storyblok-react'

export default (props) => (
  <SbEditable content={props.content}>
    <div className="mailing_list">
      <span className="mailing-title">SUBSCRIBE TO OUR MAILING LIST</span>
      <div className="mailing-separator"/>
      {/*<div className="form">
        <input type="email"/>
        <button>SUBSCRIBE</button>
      </div>*/}

      <div id="mc_embed_signup">
        <form action="https://jlhcapitalpartners.us17.list-manage.com/subscribe/post?u=b228d3f1d70528880b32a4617&amp;id=ae934047c2" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
          <div id="mc_embed_signup_scroll">
            <div class="mc-field-group" id="mc-field-group">
        	   <input required placeholder="" type="email" name="EMAIL" class="required email" id="mce-EMAIL"/>
            </div>

          	<div id="mce-responses" class="clear">
          		<div class="response" id="mce-error-response" style={{display: 'none'}}></div>
          		<div class="response" id="mce-success-response" style={{display: 'none'}}></div>
          	</div>   {/* real people should not fill this in and expect good things - do not remove this or risk form bot signups*/}

            <div style={{position: 'absolute', left: '-5000px'}} aria-hidden="true"><input type="text" name="b_b228d3f1d70528880b32a4617_ae934047c2" tabindex="-1" value=""/></div>
            <div class="clear">
              <input type="submit" value="SUBSCRIBE" name="subscribe" id="mc-embedded-subscribe" class="button"/>
            </div>
          </div>
        </form>
      </div>

    </div>

    <style jsx>{`
      .mailing_list {
        flex-grow: 1;
        margin-bottom: 45px;
        margin-left: 40px;
        margin-right: 40px;
        padding-top: 29px;
        padding-bottom: 50px;
        padding-left: 47px;
        padding-right: 47px;
        border: 1px solid #B3B3B3;
        height: 100%;
      }
      @media only screen and (max-width: 1400px) {
        .mailing_list {
          display: flex;
          flex-direction: column;
          align-items: center;
          margin-left: 18px;
          margin-right: 18px;
        }
      }
      .mailing-title {
        font-size: 25px;
        color: #A0892C;
      }
      .mailing-separator {
        height: 13px;
        width: 231px;
        border-bottom: 1px solid #B3B3B3;
      }
      .form {
        display: flex;
        margin-top: 24px;
        height: 42px;
        width: 100%;
        max-width: 1000px;
      }
      .form > button, .form > input, .list-button, .list-input {
        height: 100%;
        border-radius: 0;
        border-width: 1px;
        border-style: solid;
      }
      .form > input, .list-input {
        border-color: #B3B3B3;
        width: 100%;
        font-size: 15px;
      }
      .form > button, .list-button {
        padding-top: 9px;
        padding-bottom: 9px;
        padding-left: 40px;
        padding-right: 40px;
        border-color: #A0892C;
        background-color: #A0892C;
        color: white;
        font-family: 'Lato', sans-serif;
        font-size: 20px;
      }
      /* MOBILE LAYOUT */
      @media only screen and (max-width: 560px) {
        .mailing_list {
          padding-left: 30px;
          padding-right: 30px;
        }
        .form > button, .list-button {
          padding-left: 10px;
          padding-right: 10px;
        }
        .form > input, .list-input {
          width: calc(100% - 128px);
        }
      }
    `}</style>
  </SbEditable>
)
