import Components from './index'
import SbEditable from 'storyblok-react'

import ImageGallery from 'react-image-gallery';

const Carousel = (props) => {

  const images = props.content.images.map( (image) => {
    return (
      {
        original: image.filename,
        thumbnail: image.filename,
      }
    );
  });

  return (
    <SbEditable content={props.content}>
      <div className="carousel">

        <div className="slider">

          <ImageGallery
            items={images}
            showNav={true}
            showFullscreenButton={false}
            showPlayButton={false}
          />

        </div>

      </div>

      <style jsx global>{`
        .slider {
          margin-top: 18px;
          width: 100%;
          position: relative;
          overflow-x: hidden;
        }
        .image-gallery-slide-wrapper {
          height: 515px;
          max-height: 50vw;
        }
        .image-gallery-slide {
          position: absolute;
          width: 100%;
        }
        .image-gallery-slide > div > img {
          width: 100%;
          height: 515px;
          max-height: 50vw;
          object-fit: cover;
        }
        .image-gallery-thumbnails-container {
          margin-top: 18px;
        }
        .image-gallery-thumbnail, .image-gallery-thumbnail * {
          padding-left: 0;
          margin-right: 15px;
          width: 177px;
          height: 86px;
          border: 0px;
        }
        .image-gallery-left-nav, .image-gallery-right-nav {
          position: absolute;
          margin-top: calc( (515px / 2) - 60px );
          width: 110px;
          height: 120px;
          z-index: 5;
          border: none;
          border-radius: 0 10px 10px 0;
          background-color: rgba(0, 0, 0, 0.5);
        }
        .image-gallery-right-nav {
          margin-left: calc(100% - 110px);
          border-radius: 10px 0 0 10px;
        }
        .image-gallery-left-nav > svg, .image-gallery-right-nav > svg {
          width: 58px;
          height: 116px;
          color: white;
        }
        .image-gallery-thumbnail-image, .image-gallery-image {
          object-fit: contain !important;
        }


        /* MOBILE LAYOUT */
        @media only screen and (max-width: 1092px) {
          .image-gallery-left-nav, .image-gallery-right-nav {
            display: none;
          }

          .image-gallery-slide.center {
            margin-left: 18px;
            margin-right: 18px;

            /* Images stipe from within the border not the side of the page
            overflow-x: hidden;*/
          }
          .image-gallery-thumbnails-wrapper  {
            margin-left: 18px;
            margin-right: 18px;
            overflow-x: hidden;
          }
          .image-gallery-thumbnails {
            height: 70px;
            width: calc(-36px + 100vw);
          }
          .image-gallery-thumbnails-container {
            display: flex;
            transition: transform 450ms ease-out 0s;
          }
          .image-gallery-thumbnail, .image-gallery-thumbnail * {
            width: 120.9px;
            height: 59px;
            object-fit: cover;
          }
          .image-gallery-slide > div > img {
            width: calc(100% - 36px);
          }
        }
      `}</style>
    </SbEditable>
  );
}

export default Carousel;
