import Components from './index'
import SbEditable from 'storyblok-react'

export default (props) => (
  <SbEditable content={props.content}>
    <div className="property_info">
      {props.content.body.map((blok) =>
        Components(blok)
      )}
    </div>

    <style jsx>{`
      .property_info {
        margin-left: 20px;
        margin-right: 20px;
        margin-bottom: 15px;
        padding-top: 27px;
        padding-left: 43px;
        padding-right: 0;
        width: 560px;
        /*min-height: 726px;*/
        flex-grow: 4;
        border: 1px solid #B3B3B3;
        border-top: 10px solid #B3B3B3;
      }
      /* MOBILE LAYOUT */
      @media only screen and (max-width: 1400px) {
        .property_info {
          margin-left: 9px;
          margin-right: 9px;
          padding-left: 0;
          padding-right: 0;
        }
      }
      /* FIX HEIGHT */
      @media only screen and (max-width: 1736px) {
        .property_info {
          min-height: 0;
        }
      }
    `}</style>
  </SbEditable>
)
